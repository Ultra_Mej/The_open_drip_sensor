include <shared-definition.scad>

module Bottle_holder_500mL() {
    H_bottle=170;
    D_bottle=68;
    d_bottle=30;
    wall=3;
    
    difference(){
        cylinder(d=D_bottle+2*wall,h=H_bottle);
        //bottle body
        translate([0,0,wall]) cylinder(d=D_bottle,h=TH);
        //bottle head
        cylinder(d=d_bottle,h=TH,center=true);
        //rope holes
        translate([0,0,H_bottle-6])  rotate([90,0,0]) cylinder(d=6,h=TH,center=true);
        
        //trim
        e_trim=20;
        translate([0,0,TH/2+e_trim])  cube([TH,e_trim,TH],center=true);
    }
}

translate([0,200,0]) Bottle_holder_500mL();



module Bottle_holder_1L() {
    H_bottle=180;
    D_bottle=84;
    d_bottle=60;
    wall=2;
    
    difference(){
        cylinder(d=D_bottle+2*wall,h=H_bottle);
        //bottle body
        translate([0,0,wall]) cylinder(d=D_bottle,h=H_bottle,center=false);
        //bottle head
        cylinder(d=d_bottle,h=TH,center=true);
        //rope holes
        translate([0,0,H_bottle-6])  rotate([90,0,0]) cylinder(d=6,h=TH,center=true);
        
        //trim
        e_trim=50;
        translate([0,0,H_bottle/2+e_trim])  cube([TH,e_trim,H_bottle],center=true);
    }
    
    //renfort base
    //$fa = 1;
    translate([0,0,wall]) 
    rotate_extrude(angle=360) {
        translate([D_bottle/2,0]) circle(r=wall,$fn=4);
    }
}

!Bottle_holder_1L();