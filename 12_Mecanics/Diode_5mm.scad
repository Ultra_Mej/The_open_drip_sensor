include <shared-definition.scad>

//Diode (LED or PhD) in "5mm" package aka "T1-3/4"
////////////////////////////////////////////////////////////////////////

D_LED=5;   //head diameter
H_LED=8.7; //heighth from base
d_LED=5.8; //base diameter
h_LED=1;   //base heighth

L_pins=25; //length of legs

module Diode_5mm(){
    //body
    cylinder(d=D_LED,h=H_LED-D_LED/2,center=false);
    //head
    translate([0,0,H_LED-D_LED/2]) sphere(d=D_LED);
    //legs
    for(m=[-1,+1]) translate([m*inch/10/2,0,-L_pins])
        cylinder(d=0.5,h=L_pins,center=false);
    //base
    difference(){
        cylinder(d=d_LED,h=h_LED,center=false);
        //meplat
        translate([TH/2+D_LED/2,0,0]) cube([TH,TH,TH],center=true);
    }
}

Diode_5mm();