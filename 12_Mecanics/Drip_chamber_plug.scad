include <shared-definition.scad>

//plug that press-fit onto the "A65" drip chamber of BD
//The drop is formed on the origine
////////////////////////////////////////////////////////////////////////


echo("plug's body size. X,Y,Z=",X,Y,Z);

module Drip_chamber_plug(){
    
    //tube that forms the drop
    %cylinder(d=2,h=h_drop_tube,center=false);
    //drop
    V_drop=1000/20; //drop volume in µl aka mm3 (20 drops per mL)
    r_drop=(V_drop*3/4/pi)^(1/3); //drop radius (assuming the drop is a sphere of volume 4/3.pi.r^3)
    echo("r_drop=",r_drop);
    %translate([0,0,-r_drop]) sphere(r=r_drop);
    //plug
    difference(){
        //body
       translate([0,Y/2-X/2,Z/2-15]) cube([X,Y,Z],center=true);
        //ring slot
        translate([0,-TH/2,h_drop_tube-e_ring/2])
            cube([D_ring,TH,e_ring],center=true);
        translate([0,0,h_drop_tube-e_ring/2])
            cylinder(h=e_ring,d=D_ring,center=true);
        
        //space
        translate([0,-TH/2,0])
            cube([D_ring-2*grip,TH,TH],center=true);
        translate([0,0,h_drop_tube-e_ring/2])
            cylinder(h=TH,d=D_ring-2*grip,center=true);
        
       //'extra' slit
       translate([0,-TH/2+D_ring/2,h_drop_tube-1.5/2])
            cube([TH,TH,1.5],center=true);
    }
}

Drip_chamber_plug();




    
 
