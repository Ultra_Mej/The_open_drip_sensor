TH=200;
$fn=100;

inch=25.4;
pi=3.1416;

M3=3;
M3_screw=2.8;
M3_through=3.2;

M2=2;
M2_screw=1.8;
M2_through=2.2;

//'ring' parameters
////////////////////////////////////////////////////////////////////////
D_ring=20;
e_ring=8;
grip=1;

h_drop_tube=10;     //height of the drip tube



//dimension of the Drip_chamber_plug body: X,Y,Z
////////////////////////////////////////////////////////////////////////
X=D_ring+2*8;
extra_wall=2;
Y=30+extra_wall;
Z=h_drop_tube+5+15;