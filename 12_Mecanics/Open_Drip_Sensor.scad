include <shared-definition.scad>


use <Bottle_holder.scad>
%translate([0,0,50]) rotate(90) Bottle_holder_1L();



//drip chamber plug
////////////////////////////////////////////////////////////////////////
use <Drip_chamber_plug.scad>

D_Diode_hole=5+0.2; //LED/PhD hole diameter
D_Diode_base=5.8+0.2; //LED/PhD base hole diameter

deltaZ_to_drop=5; //The light beam is 5mm under the drip formation
PCB_holes_c2c=[28,12]; //PCB holes (center to center)

module Body(){
    //Drip_chamber_plug with LED and PCB holes
    difference(){
        translate([0,0,deltaZ_to_drop]) Drip_chamber_plug();
        
        //diode holes
        rotate([0,90,0]) cylinder(d=D_Diode_hole,h=TH,center=true);
        //diode base poket
        for(m=[-1,+1]) rotate([0,m*90,0])
            translate([0,0,X/2-1]) cylinder(d=D_Diode_base,h=TH,center=false);
        
        //PCB holes
        for(i=[-1,+1],j=[-1,+1])
            translate([i*PCB_holes_c2c[0]/2,0,j*PCB_holes_c2c[1]/2])
                rotate([90,0,0]) cylinder(d=M2_screw,h=TH,center=true);
        
        //Screen hole
        translate([-PCB_holes_c2c[0]/2,-10,0])
            rotate([90,0,0]) cylinder(d=M2_screw,h=TH,center=false);
    }
    //Screen handle
    difference(){
        union(){
            //cylinder
            for(m=[-1,+1]) translate([-PCB_holes_c2c[0]/2-10,-18,m*10])
                    rotate([90,0,0]) cylinder(d=4-0.2,h=4,center=false);
            //side handle
            hull(){
                //front
                translate([-10/2-X/2,-18+2/2,0]) cube([10,2,20+4],center=true);
                //back
                translate([-X/2+1/2,-5,0]) cube([1,1,10],center=true);
            }
        }
        //trim
        translate([-TH/2-PCB_holes_c2c[0]/2-10-4/2+1,0,0]) cube([TH,TH,TH],center=true);
    }
}

Body();


//Diodes
////////////////////////////////////////////////////////////////////////

use <Diode_5mm.scad>
module LED() color("red",alpha=0.8)  Diode_5mm();
module PhD() color("blue",alpha=0.8) Diode_5mm();

 translate([+X/2,0,0]) rotate([0,-90,0]) LED();
 translate([-X/2,0,0]) rotate([0,+90,0]) PhD();



//PCB
////////////////////////////////////////////////////////////////////////
module PCB() color("green") render() 
    difference(){
        cube([X+2*7,20,1.6],center=true);
        echo("PCB shape=",X+2*7,"x",20);
        //mark for center
        cylinder(d=1,h=TH,center=true);
        //mark for Diode footprint
        for(m=[-1,+1]) translate([m*(X+1)/2,0,0]) cube([1,5,TH],center=true);
        //PCB holes
        for(i=[-1,+1],j=[-1,+1])
            translate([i*PCB_holes_c2c[0]/2,j*PCB_holes_c2c[1]/2,0])
                cylinder(d=M3_through,h=TH,center=true);
    }
    
*PCB();
//PCB in position
translate([0,20,0]) rotate([90,0,0]) PCB();
    
    
//Screen https://wiki.seeedstudio.com/Grove-OLED-Yellow&Blue-Display-0.96-(SSD1315)_V1.0/
////////////////////////////////////////////////////////////////////////
module Screen(){
    //outline
    color("blue",alpha=0.8) render() difference(){
        
        translate([-1.8,-20/2,0]) cube([42.5,20,2],center=false);
        //holes
        cylinder(d=M2_through,h=TH,center=true);
        translate([10,-10,0]) cylinder(d=4,h=TH,center=true);
        translate([10,+10,0]) cylinder(d=4,h=TH,center=true);
        translate([30,-10,0]) cylinder(d=M2_through,h=TH,center=true);
        translate([30,+10,0]) cylinder(d=M2_through,h=TH,center=true);
    }
   
    //screen
    %translate([12.5,-16.5/2,0]) cube([24.5,16.5,4],center=false);
}

*Screen();
//Screen in position
translate([-PCB_holes_c2c[0]/2,-20,0]) rotate([90,180,0]) Screen();

//Batterie holder
////////////////////////////////////////////////////////////////////////
use <Batterie_holder.scad>
translate([0,25,-PCB_holes_c2c[1]/2]) rotate([90,0,180]) color("purple") Batterie_holder();

  
////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////  
    
//Export STL
*union(){
    translate([0,X/2,Y-X/2]) rotate([-90,0,0]) Body();
    Batterie_holder();
}
    
//Export DXF
*projection() PCB();