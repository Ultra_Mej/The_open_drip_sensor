include <shared-definition.scad>


//Batterie
////////////////////////////////////////////////////////////////////////
translate([20,0,0]){
    //18650 cell
    %translate([0,0,0]) cylinder(d=18,h=65,center=true);
    //AA
    %translate([0,-20,0]) cylinder(d=14,h=50,center=true);
    //AAA
    %translate([0,-40,0]) cylinder(d=10.5,h=44.5,center=true);
}


//dupont wire, male pin
////////////////////////////////////////////////////////////////////////
module dupont_pin() {
    //handle
    handle_play=0.1;
    translate([0,0,14/2]) cube([inch/10+handle_play,inch/10+handle_play,14],center=true);
    //pin
    pin_play=0.3;
    cube([0.8+pin_play,0.8+pin_play,6*2],center=true);
}
*translate([-20,0,0]) dupont_pin();

//dupont pins, in position
module dupont_pins(){
    translate([0,0,+H/2]) translate([0,-e_top/2,+e_top/2]) rotate([45,   0,0]) dupont_pin(); //top
    translate([0,0,-H/2]) translate([0,-e_top/2,-e_top/2]) rotate([45+90,0,0]) dupont_pin(); //bottom
}
%dupont_pins();


//18650 Batterie holder
////////////////////////////////////////////////////////////////////////
D=18;
H=65;
e_top=5;
e_back=2.5;
//complient press-fit:
e=2; //wall thickness
delta_compliance=D/2*0.25; //percentage of the 2nd_halfe of the batterie, that is held by the holder

module 18650_holder() difference(){
    //body
    union(){
        translate([0,D/2+e_back/2,0]) cube([D+2*e,e_back,H+2*e_top],center=true);
        translate([0,e/2,0]) cylinder(d=D+2*e,h=H+2*e_top,center=true);
    }
    
    //18650 cell
    cylinder(d=D,h=H,center=true);
    
    //trim front
    translate([0,-TH/2-delta_compliance,0]) cube([TH,TH,H],center=true);
    //slits for compliant press-fit
    for(m=[-1,-0.5,0,+0.5,+1]) translate([0,-TH/2+D/2,m*(H-e)/2])
        cube([TH,TH,e],center=true);
    
    //dupont pins poket
    dupont_pins();
    //slit for dupont pin
    for(m=[-1,+1]) translate([0,0,m*H/2])
        cube([1,TH,0.8],center=true);
    
    //trim front
    translate([0,-TH/2-D/2+1,0]) cube([TH,TH,TH],center=true);
    
    //trim back
    translate([0,TH/2+D/2+e_back,0]) cube([TH,TH,TH],center=true);
    
    //pop-out hole
    D_pop=8;
    for(i=[-1,0,1]) translate([0,0,i*(H/2-D_pop)])
        rotate([90,0,0]) cylinder(d=D_pop,h=TH,center=true);
    
    
}
*18650_holder();

//Batterie holder for Open_drip sensor
////////////////////////////////////////////////////////////////////////
L=5; //distance from Holder-edge to PCB-holes
e_handle=e_back; //handle thickness
module Batterie_holder() translate([0,-D/2-e-L]) {
    rotate([-90,0,90]) translate([0,-D/2-e_back,0]) 18650_holder();
    
    //handle for attachement on PCB
    for (m=[-1,+1]) translate([m*28/2,0,0]) difference(){
        translate([0,L/2+D/2+e,e_handle/2]){
            cube([4,L,e_handle],center=true);
            translate([0,L/2,0]) cylinder(d=4,h=e_handle,center=true);
        }
        //hole
        translate([0,D/2+e+L,0]) cylinder(d=M2_through,h=TH,center=true);
    }
}

!translate([-H/2-e_top-D,0,0]) Batterie_holder();
    

