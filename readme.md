[[_TOC_]]
_________________________________________________________

# ODS-Project presentation and history

![ODS exploded view](15_pictures/IMG_20220505_090224.jpg)  


The **Open-Drip-Sensor** (ODS) is a frugal scientific instrument that counts and display the drip-rate of an [intravenous line](https://en.wikipedia.org/wiki/Intravenous_therapy).
It can be adapted on any kind of [drip chamber](https://en.wikipedia.org/wiki/Drip_chamber)

It is an upgrade of part of the [AIMD project](00_AIMD/AIMD_project.pdf), that I had the privilege to coach in November and December of 2021 during a prototyping internship organized by [APSA](http://www.scienceafrique.fr/) in [Fablab Digiscope](https://fablabdigiscope.universite-paris-saclay.fr).
[AIMD](00_AIMD/AIMD_project.pdf) is a Ethiopian project that aims to develop a set of 3 sensors, including a drip rate sensor, in order to help midwife in monitoring child-birth induction.

<img src="00_AIMD/Hume_Lidiya.jpg" height="300" />
<img src="00_AIMD/Lidiya_Mejdi_Hume.jpg" height="300" />

_________________________________________________________


# Open-Drip-Sensor overview

With a total [Bill of material](11_BOM/BOM.pdf) of **less than 12€**, the ODS can be manufacture by anybody having access to a 3D printer and a CNC (to engrave the single side PCB).

## Working principle

The ODS working principles is very simple:
1. An LED shines onto a Photodiode.
2. The light beam passes in the drip-chamber just where the drop forms.
3. When the drop forms the photo-signal "slowly" (10 to 50ms) decreases.
4. When the drop finally falls, the photo-signal jumps back-up.

Here under, an analysis of the raw photo-signal, measured with a [fast USB-osciloscope](https://digilent.com/reference/test-and-measurement/analog-discovery-2/start):  
@ 1s/div, we see drops falling every 1 to 2 seconds:  
<img src="15_pictures/Photosignal_10k_1s-per-div.png" width="500" /> <br />
@ 200ms/div, when the "blue signal" is high a drop is being formed. When it is low the drop has fallen and hasn't yet been replaced :  
<img src="15_pictures/Photosignal_100k_200ms-per-div.png" width="500" /><br />
@ 100ms/div, zoom onto the growth and fall of a single drop:  
<img src="15_pictures/Photosignal_10k_100ms-per-div.png" width="500" /> <br />
@10ms/div, zoom onto the "rising edge" of the photo-signal, just after the fall of the drop:  
<img src="15_pictures/Photosignal_10k_10ms-per-div.png" width="500" /> <br />
NB: the rising time (less than 1ms) is actually limited by the bandwidth of the TIA.


## Demonstration

<img src="15_pictures/IMG_20220505_162601.jpg" height="600" />

<video height="600" controls loop>
  <source src="15_pictures/VID_20220505_090825_lowrez.mp4" type="video/mp4">
</video>

[video](15_pictures/VID_20220505_090825_lowrez.mp4) of the ODS in operation.


## Design and Fabrication files

The design and fabrication files for the Mechanics (OpenScad), Electronics (Kicad) and Software (Arduino) of the ODS are readily available within this repository.

### Mechanics

<img src="12_Mecanics/1.png" width="600" /> <br />
<img src="12_Mecanics/2.png" width="300" />
<img src="12_Mecanics/3.png" width="300" /> <br />
<img src="12_Mecanics/4.png" width="300" /> <br />

![ODS ready to be printed](12_Mecanics/fab/Screenshot_slicer.png)

### Electronics

[Schematics](13_Electronics/ODS_PCB_schematics.pdf)

<img src="13_Electronics/ODS_PCB_preview.png" height="180" />
<img src="13_Electronics/ODS_PCB_Cu.png" height="180" /> <br />
<img src="15_pictures/IMG_20220505_163925.jpg" width="600" /> <br />

### Firmware

ODS is driven by the beloved (and probably out-dated) ATtiny85 microcontroller: 8bit, 8kB of memory (><'), 8MHz clock (internal).

The ADC conversion are synchronized (4ms per sample) using the ISR (Interrupt service routine) on timer 1.
 
The minuscule memory of the ATtiny is not big enough to accommodate for the full [U8g2 library](https://github.com/olikraus/u8g2). It uses the U8x8 instead, which does the job.

The ATtiny85 was flashed using an [Arduino UNO](https://create.arduino.cc/projecthub/arjun/programming-attiny85-with-arduino-uno-afb829) and a [8 pin clip](https://www.pomonaelectronics.com/products/test-clips/soic-clip-8-pin).

The code is very simple and less than [200 lines](14_Software/ODS_firmware/ODS_firmware.ino)

> Sketch uses 5562 bytes (67%) of program storage space. Maximum is 8192 bytes.  
> Global variables use 329 bytes (64%) of dynamic memory, leaving 183 bytes for local variables. Maximum is 512 bytes.


## Key components of the BOM

### LED

**Package** through-hole *5mm* aka *T-1 3/4*:
- the plastic dome works as a collimating lens

**Color** *red* aka 620 to 680nm:
- Minimise tension (~2V)
- minimise cost
- Could have gone NIR (~950nm) but the beam would not be visible (eye check of alignment)

Examples of references:
- [MCL053PD](https://fr.farnell.com/multicomp-pro/mcl053pd/led-5mm-36-rouge/dp/1581136) !No more manufactured!

### PhotoDiode

**Package** through-hole *5mm* aka *T-1 3/4*:
- the plastic dome works as a focusing lens
- Spectral range: Si PhD without filter: 380-1100nm

Examples of references:
- [1540051EA3590](https://fr.farnell.com/wurth-elektronik/1540051ea3590/photodiode-non-amplifiee-5mm-940nm/dp/2990885) the cheaper choice on Farnell.fr
- [SFH 213](https://fr.farnell.com/osram-opto-semiconductors/sfh-213/photodiode-850nm-t-1-3-4/dp/2981693)

### Operational Amplifier
To build a low-speed [TIA](https://en.wikipedia.org/wiki/Transimpedance_amplifier), almost any FET input (for low current noise and input bias current) OPA would work. I choose the [MCP6401](https://www.microchip.com/en-us/product/MCP6401) because its low cost, wide voltage range (2 to 6V) and because its datasheet goes into details on how to use it as a TIA. The reading of microchip's [application note on TIA](https://www.microchip.com/en-us/application-notes/an951) is interesting too.

### MCU

I choose the [ATtiny85-20SF](https://fr.farnell.com/microchip/attiny85-20sf/mcu-8-bits-20mhz-soic-8/dp/2774978) because it has become a reference among the makers both advanced and beginners.

Also I like a challenge ^^.

![ATtiny pinout](13_Electronics/datasheets/ATtiny/attiny85_pinout.png)


## Jargon:
- BOM: Bill Of Material
- OpA: Operational Amplifier
- PhD: Photo-Diode (aka Photodiode)
- TIA: Transimpedance Amplifier
- MCU: Micro Controler Unit
- LED: Light Emmiting Diode
_________________________________________________________

# Credit

## Softwares

### R&D

- Mechanics: [Openscad](https://openscad.org/downloads.html)
- Electronics: [Kicad](https://www.kicad.org/download)
- Firmware: [Arduino IDE](https://www.arduino.cc/en/software)
- Firmware library:[ATtiny core support](https://github.com/damellis/attiny) for Arduino IDE and [U8x8](https://github.com/olikraus/u8g2) for the communication with the display. 

### Desktop tool
- Linux Manjaro OS, KDE Desktop
- Git
- LibreOffice Calc and Ghostwriter with Hunspell dictionary
- Inkscape, VLC

## About the author

<img src="15_pictures/Mejdi_ID_pic.png" height="200" />
<img src="15_pictures/IMPACT PHOTONICS logo.png" height="200" />

[Mejdi](https://fr.linkedin.com/in/mejdi-nciri-6a0b03a) holds a master of engineering in optical science, after which he specialized in optical-spectroscopy.
From 2012 to 2017 he was CEO of a start-up who developed a disruptive spectrometer, with a very aggressive (and expensive) patent strategy.
Since, Mejdi has shifted toward open-source-software and open-source-hardware. In 2018 he graduate from MIT's fabacademy program, and has now become an active member of the Fablab communities.

Mejdi's company, [IMPACT PHOTONICS](http://www.impact-photonics.com/), has core expertise in **functional design analysis**, **frugal engineering**, **optics**, **micro-electronics**, **micro-mechanics**, **data-analysis**, **digital fabrication** and **rapid prototyping**.

IMPACT PHOTONICS internal projects focus on engineering open-source and cost-effective optical scientific instrumentation in projects ranging from *UV-Vis time-resolved absorption-spectroscopy for blood-chemistry-analysis* to *IR reflectance-spectroscopy for plastic-sorting*.

**IMPACT PHOTONICS also offers custom prototyping services to its clients** in projects ranging from *Multi-spectral imagery for bacterial colony picking* to *OD-meter for 96-well-plate analysis*.

Contact: mejdi@impact-photonics dot com


_________________________________________________________
# To Do

#### Upgrades:
- [ ] Do a Through-hole PCB version (easier to manufacture in Ethiopia)
- [ ] Give more details *Design and Fabrication files* section
- [ ] LDO 3V, in order to get a constant timer and LED current while battery discharges.
- [ ] Measure battery tension to indicate charge status
- [ ] Constant current source
- [ ] Buzzer?




