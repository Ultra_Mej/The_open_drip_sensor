/*
Mars 2022
by Mej
*/


//ATTiny 85 pins configuration
//////////////////////////////////////////////////////
#define ADC A2  //signal  to monitor (Photodiode D2 through TIA) on A1 aka PB4 (pin3) 
#define SCL 2 //Clock software-I2C (pin7)
#define SDA 0 //Data software-I2C (pin5)
#define LED 1   //red led (D1) on PB1 (pin6). Active low.
#define LED_G 3 //green led (D0) on PB3 (pin2). Active low. ( ? can NOT be a PWM if PB4 used as input ?)
bool ON=0;
bool OFF=1;


//OLED screen setup, see https://github.com/olikraus/u8g2
//////////////////////////////////////////////////////
#include <U8x8lib.h>
U8X8_SSD1306_128X64_NONAME_SW_I2C u8x8(SCL, SDA, U8X8_PIN_NONE); //https://github.com/olikraus/u8g2/wiki/u8x8setupcpp

//Global variables
//////////////////////////////////////////////////////
unsigned long t0=0; //time since start in ms
unsigned int loop_cnt=0; //loop count

unsigned int drop_cnt=0;

unsigned int t=0; //time of last drop
unsigned int delta_t_drops[3]={0,0,0}; //time between last drops
unsigned int delta_t_drop_avg=(delta_t_drops[0]+delta_t_drops[1]+delta_t_drops[2])/3;

unsigned int drop_rate=0; //drops per minute


int val_ref=0; //ADC value
int val_a=0; //ADC value
int val_b=0; //ADC value
int D4ms=-2000; //derivative of the photosignal over 4ms

bool drop_flag=0;



//////////////////////////////////////////////////////
//Setup
//////////////////////////////////////////////////////
void setup() {

  //pin modes
  pinMode(LED_G, OUTPUT); 
  pinMode(LED, OUTPUT); 
  pinMode(ADC, INPUT);

  //LEDs blink for 2secondes
  for(int i=0;i<20;i++){
      digitalWrite(LED_G, ON); 
      digitalWrite(LED, ON); 
      delay(50);
      digitalWrite(LED_G, OFF); 
      digitalWrite(LED, OFF); 
      delay(50);
  }

  digitalWrite(LED, ON);
  digitalWrite(LED_G, OFF);
  val_a=analogRead(ADC); 
  val_b=analogRead(ADC);
  D4ms=(val_b-val_a);
  
  //Screen setup
  u8x8.begin();
  u8x8.setFlipMode(0);
  u8x8.setContrast(128);
  u8x8.setFont(u8x8_font_amstrad_cpc_extended_r); //8x8pixel font on 128x64pixel screen => 16 caracters x 8 lignes
  u8x8.drawString(0,0,"OPEN DRIP SENSOR");
  u8x8.drawString(0,1," vQ1 2022 by Mej");
  u8x8.drawString(0,2,"----------------");
  u8x8.drawString(0,3,"     drop/min   ");  //drop_rate @ (0,3)
  u8x8.drawString(0,4,"----------------");
  u8x8.drawString(0,5,"Since last reset"); 
  u8x8.drawString(0,6," Secondes=      "); //time @ (10,6)
  u8x8.drawString(0,7," Drops=         "); //drops count @ (7,7)

  //get reference voltage (should be around 100mV and 5000mV ie ADC~20 to 1000 counts)
  delay(1000); //10ms delay with current timer setup (8MHz
  val_ref = analogRead(ADC);

  //set timer1 interrupt at 250Hz <=> 4ms (Attiny85, 8MHz)
  //////////////////////////////////////////////////////////////////////////
  TCNT1  = 0;//initialize counter value to 0
  //TCCR (Timer Counter Control Register)
  TCCR1 = B10001000; //mode of operation is CTC (Clear Timer on Compare), prescaler is 1024 (8MHz/1024=7.8125kHz <=> 128us )
  //Timer Interrupt MaSK Register
  TIMSK=B1000000;//output compare interrupt enabled
}


//////////////////////////////////////////////////////
//Loop
//////////////////////////////////////////////////////
void loop() {
  
  //did a drop fall?..
  if(drop_flag==1){
    digitalWrite(LED_G,ON);
    
    //update drop_cnt
    drop_cnt+=1;
    
    //update drop_rate
    delta_t_drop_avg = (delta_t_drops[0]+delta_t_drops[1]+delta_t_drops[2])/3;
    drop_rate=60000/delta_t_drop_avg;

    //refresh drop_cnt
    u8x8.setCursor(7, 7);
    u8x8.print(drop_cnt); 
    
    //refresh drop_rate
    u8x8.setCursor(1, 3);
    u8x8.print(drop_rate);
    //clear last caracters of drop-rate if necessary
    if(drop_rate<10){
      u8x8.drawString(2,3,"  "); 
    }
    else if(drop_rate<100){
      u8x8.drawString(3,3," "); 
    }

    //end of drop fall
    drop_flag=0;
    digitalWrite(LED_G,OFF);
  }

  //end of loop
  //////////////////////////////////////////////////////
  
  //refresh time
  u8x8.setCursor(10, 6);
  u8x8.print(t0/1000); 

  if(t>6000){ //if no drops for more than 6 secondes
    u8x8.drawString(1,3,"xxx"); //drop rate
  }

  loop_cnt+=1;
}


// Interrupt service routine for timer1
//NB:do NOT digitalWrite here, or the screen display will bug...
ISR(TIMER1_COMPA_vect){
  //increment time of 4ms ~ "milli()" 
  t0+=4;
  t+=4;
  
  //update ADC values
  val_a = val_b;
  val_b = analogRead(ADC);
  D4ms=(val_b-val_a);
  
  //if no drop is falling
  if(drop_flag==0){
    //did a drop fall?..
    if(D4ms>val_ref/4){
      drop_flag=1;
      //update delta_t_drops
      delta_t_drops[drop_cnt%3]=t;
      //reset t
      t=0; 
    }
  }
   
}
