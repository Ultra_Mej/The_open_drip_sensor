//ATTiny 85 pins configuration
//////////////////////////////////////////////////////
#define LED 1   //red led (D1) on PB1 (pin6). Active low.
#define ADC A2  //signal  to monitor (Photodiode D2 through TIA) on A1 aka PB4 (pin3) 
#define LED_G 3 //green led (D0) on PB3 (pin2). Active low. ( ? can NOT be a PWM if PB4 used as input ?)
#define SCL 2 //Clock software-I2C (pin7)
#define SDA 0 //Data software-I2C (pin5)

bool led=0;
bool led_g=0;


  
void setup() {

  //pin modes
  pinMode(LED_G, OUTPUT); 
  pinMode(LED, OUTPUT); 
  pinMode(ADC, INPUT);

  //LEDs blink for 2secondes
  for(int i=0;i<10;i++){
      digitalWrite(LED_G, 0); 
      digitalWrite(LED, 0); 
      delay(200);
      digitalWrite(LED_G, 1); 
      digitalWrite(LED, 1); 
      delay(200);   
  }

  
//  //set timer0 interrupt at 200Hz (5ms). See datasheet p100/234 for details.
//  //////////////////////////////////////////////////////////////////////////
//  //TCNT (Time CouNTer)
//  TCNT0  = 0;//initialize counter value to 0
//  //TCCR (Timer Counter Control Register)
//  TCCR0A = B10; //mode of operation is CTC (Clear Timer on Compare)
//  TCCR0B = B101; //prescaler is 1024 (8MHz/1024=7.8125kHz <=> 128us )
//  //OCR (Output Compare Register)
//  OCR0A = 20;//20*267.4us = 5.3ms  => core freq after pre-scaler is 3.74kHz
//  //Timer Interrupt MaSK Register
//  TIMSK=B10000;

  //set timer1 interrupt at 250Hz (4ms). See datasheet p100/234 for details.
  //////////////////////////////////////////////////////////////////////////
  //TCNT (Time CouNTer)
  TCNT1  = 0;//initialize counter value to 0
  //TCCR (Timer Counter Control Register)
  TCCR1 = B10001000; //mode of operation is CTC (Clear Timer on Compare), prescaler is 1024 (8MHz/1024=7.8125kHz <=> 128us )
  //OCR (Output Compare Register)
  //OCR1A = 10;//39*128us ~ 5ms  => core freq after pre-scaler is 3.74kHz
  //Timer Interrupt MaSK Register
  TIMSK=B1000000;//output compare interrupt enabled

}


void loop() {
  led_g=!led_g;
  digitalWrite(LED_G, led_g);
  delay(10000); //100ms delay
}


// Interrupt service routine for timer0
ISR(TIMER1_COMPA_vect)
{
  led=!led;
  digitalWrite(LED, led);
}
