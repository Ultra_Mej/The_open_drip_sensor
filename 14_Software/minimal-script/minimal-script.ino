/*
Mars 2022
by Mej
*/


//ATTiny 85 pins configuration
//////////////////////////////////////////////////////
#define LED 1   //red led (D1) on PB1 (pin6). Active low.
#define ADC A2  //signal  to monitor (Photodiode D2 through TIA) on A1 aka PB4 (pin3) 
#define LED_G 3 //green led (D0) on PB3 (pin2). Active low. ( ? can NOT be a PWM if PB4 used as input ?)
#define SCL 2 //Clock software-I2C (pin7)
#define SDA 0 //Data software-I2C (pin5)

//OLED screen setup, see https://github.com/olikraus/u8g2
//////////////////////////////////////////////////////
#include <U8x8lib.h>
U8X8_SSD1306_128X64_NONAME_SW_I2C u8x8(SCL, SDA, U8X8_PIN_NONE); //https://github.com/olikraus/u8g2/wiki/u8x8setupcpp

//Global variables
//////////////////////////////////////////////////////
unsigned long t; //time
int loop_cnt=0; //loop count
int val=0; //ADC value



//////////////////////////////////////////////////////
//Setup
//////////////////////////////////////////////////////
void setup() {

  //pin modes
  pinMode(LED_G, OUTPUT); 
  pinMode(LED, OUTPUT); 
  pinMode(ADC, INPUT);

  //LEDs blink for 2secondes
  for(int i=0;i<20;i++){
      digitalWrite(LED_G, 0); 
      digitalWrite(LED, 0); 
      delay(50);
      digitalWrite(LED_G, 1); 
      digitalWrite(LED, 1); 
      delay(50);   
  }

  //Screen setup
  u8x8.begin();
  u8x8.setFlipMode(0);
  u8x8.setContrast(128);
  u8x8.setFont(u8x8_font_amstrad_cpc_extended_r); //8x8pixel font on 128x64pixel screen => 16 caracters x 8 lignes
  u8x8.drawString(0,0,"OPEN DRIP SENSOR");
  u8x8.drawString(0,1," vQ1 2022 by Mej");
  u8x8.drawString(0,2,"----------------");
  u8x8.drawString(0,3,"ADC=            "); //write current ADC value @ (6,3)
  u8x8.drawString(0,4,"Since sart-up:  "); 
  u8x8.drawString(0,5,"        secondes"); //write time @ (0,5)
  u8x8.drawString(0,6,"           loops"); //write loop count @ (0,6)
  u8x8.drawString(0,7,"                ");
  
  analogWrite(LED, 0); //RED LED @ 100% 
  digitalWrite(LED_G, 1); //green LED OFF
}


//////////////////////////////////////////////////////
//Loop
//////////////////////////////////////////////////////
void loop() {

  //beginning of loop
  //////////////////////////////////////////////////////
  digitalWrite(LED_G, 0); //green LED ON
  
  t = millis();
  val = analogRead(ADC);


  //end of loop
  //////////////////////////////////////////////////////
  digitalWrite(LED_G, 1); //green LED OFF

  //update screen
  
  if (val>=100){
    u8x8.setCursor(6, 3);
  }
  else if(val>=10){
    u8x8.drawString(6,3,"0"); 
    u8x8.setCursor(7, 3);
  }
  else { //then val<10
    u8x8.drawString(6,3,"00"); 
    u8x8.setCursor(8, 3);
  }
  u8x8.print(val); 
  
  u8x8.setCursor(0, 5);
  u8x8.print(t/1000); 
  
  u8x8.setCursor(0, 6);
  u8x8.print(loop_cnt); 
  
  loop_cnt+=1;
}
